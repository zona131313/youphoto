CREATE DATABASE  IF NOT EXISTS `youphotoschema` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `youphotoschema`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: youphotoschema
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'Manager');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (2,1,21),(3,1,22),(4,1,23),(1,1,24);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add delivery',1,'add_delivery'),(2,'Can change delivery',1,'change_delivery'),(3,'Can delete delivery',1,'delete_delivery'),(4,'Can view delivery',1,'view_delivery'),(5,'Can add service',2,'add_service'),(6,'Can change service',2,'change_service'),(7,'Can delete service',2,'delete_service'),(8,'Can view service',2,'view_service'),(9,'Can add status',3,'add_status'),(10,'Can change status',3,'change_status'),(11,'Can delete status',3,'delete_status'),(12,'Can view status',3,'view_status'),(13,'Can add place',4,'add_place'),(14,'Can change place',4,'change_place'),(15,'Can delete place',4,'delete_place'),(16,'Can view place',4,'view_place'),(17,'Can add picture',5,'add_picture'),(18,'Can change picture',5,'change_picture'),(19,'Can delete picture',5,'delete_picture'),(20,'Can view picture',5,'view_picture'),(21,'Can add order',6,'add_order'),(22,'Can change order',6,'change_order'),(23,'Can delete order',6,'delete_order'),(24,'Can view order',6,'view_order'),(25,'Can add permission',7,'add_permission'),(26,'Can change permission',7,'change_permission'),(27,'Can delete permission',7,'delete_permission'),(28,'Can view permission',7,'view_permission'),(29,'Can add group',8,'add_group'),(30,'Can change group',8,'change_group'),(31,'Can delete group',8,'delete_group'),(32,'Can view group',8,'view_group'),(33,'Can add user',9,'add_user'),(34,'Can change user',9,'change_user'),(35,'Can delete user',9,'delete_user'),(36,'Can view user',9,'view_user'),(37,'Can add content type',10,'add_contenttype'),(38,'Can change content type',10,'change_contenttype'),(39,'Can delete content type',10,'delete_contenttype'),(40,'Can view content type',10,'view_contenttype'),(41,'Can add log entry',11,'add_logentry'),(42,'Can change log entry',11,'change_logentry'),(43,'Can delete log entry',11,'delete_logentry'),(44,'Can view log entry',11,'view_logentry'),(45,'Can add session',12,'add_session'),(46,'Can change session',12,'change_session'),(47,'Can delete session',12,'delete_session'),(48,'Can view session',12,'view_session');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$150000$K64NGh0Ws3sm$JapF1weJ8FntfH+oWR77C3FCRmhFiB18EYPmYreUPf0=','2019-04-10 20:31:28.239009',1,'admin','','','admin@admin.admin',1,1,'2019-04-09 17:53:31.225384'),(2,'pbkdf2_sha256$150000$YMmPlQIl0Lu6$civvfmSSpwIPumg0bF8ccAT/a4b85mw2HkoLGUTW9hc=','2019-04-10 20:53:05.073219',0,'User1','','','',0,1,'2019-04-09 18:36:03.373098'),(3,'pbkdf2_sha256$150000$HUge44p6tQxz$eThxeRGshv5D0dmGJGvQicEjMXnaPPgrAg6SmO0R+hs=','2019-04-10 19:17:26.660488',0,'User3','','','',0,1,'2019-04-10 17:26:08.130853'),(4,'pbkdf2_sha256$150000$l4Vq0Rw5WumF$tzPZxHxS0v/L/6iMSnJm1Vea+9C7PB0EBmIs+S3EPho=','2019-04-10 19:19:01.311447',0,'User4','','','',0,1,'2019-04-10 17:41:14.578720'),(5,'pbkdf2_sha256$150000$VLVVcgcERj1y$895H4aOvpTEXbIPoHwoBwc0i+0dlsY+CiSONsqgiFYI=','2019-04-10 20:56:52.332656',0,'Manager1','','','',0,1,'2019-04-10 18:59:32.000000'),(6,'pbkdf2_sha256$150000$SkmI5UhA2FgR$0SamKWq4membEhtFectw8Xcz/NyUfLGJRMibD2UY4t4=',NULL,0,'Manager2','','','',0,1,'2019-04-10 18:59:44.000000'),(7,'pbkdf2_sha256$150000$YMKzjENNLjz8$7azNV1ipcIziR0bGp1aMNOXz6kaDpC21WWEPBhnmrrg=','2019-04-10 19:15:06.193293',0,'User2','','','',0,1,'2019-04-10 19:02:43.341928'),(8,'pbkdf2_sha256$150000$YcPFcbsJ7gtf$FSkMOOC6repU8vwtajA0LsN+3V0o/wI2uh3QwTj7AIM=',NULL,0,'User5','','','',0,1,'2019-04-10 19:03:00.112712');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (1,5,1),(2,6,1);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (6,5,21),(7,5,22),(8,5,23),(5,5,24),(2,6,21),(3,6,22),(4,6,23),(1,6,24);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-04-09 17:54:26.029230','1','id: 1 name: Новый',1,'[{\"added\": {}}]',3,1),(2,'2019-04-09 17:54:39.576929','2','id: 2 name: В работе',1,'[{\"added\": {}}]',3,1),(3,'2019-04-09 17:54:46.713365','3','id: 3 name: В пути',1,'[{\"added\": {}}]',3,1),(4,'2019-04-09 17:54:53.581962','4','id: 4 name: Ждет оплаты',1,'[{\"added\": {}}]',3,1),(5,'2019-04-09 17:55:01.073776','5','id: 5 name: Готов к выдаче',1,'[{\"added\": {}}]',3,1),(6,'2019-04-09 17:55:07.357093','6','id: 6 name: Завершен',1,'[{\"added\": {}}]',3,1),(7,'2019-04-09 17:55:13.519388','7','id: 7 name: Отменен',1,'[{\"added\": {}}]',3,1),(8,'2019-04-09 17:56:03.633177','1','Почтой',1,'[{\"added\": {}}]',1,1),(9,'2019-04-09 17:56:08.556462','2','Само-вывоз',1,'[{\"added\": {}}]',1,1),(10,'2019-04-09 17:56:18.212145','3','Курьером',1,'[{\"added\": {}}]',1,1),(11,'2019-04-09 17:56:44.310889','1','Печать 10x15',1,'[{\"added\": {}}]',2,1),(12,'2019-04-09 17:56:50.893510','2','Печать 4x3',1,'[{\"added\": {}}]',2,1),(13,'2019-04-09 17:57:00.632309','1','пр. Победы, дом 26',1,'[{\"added\": {}}]',4,1),(14,'2019-04-09 17:57:06.285709','2','пр. Пушкина, дом. Колотушкина',1,'[{\"added\": {}}]',4,1),(15,'2019-04-10 00:12:54.360507','11','User1: user_2/2019-04-10_030938.028628_name_DSC_0049.JPG',3,'',5,1),(16,'2019-04-10 00:12:54.364475','10','User1: user_2/2019-04-10_030933.280925_name_DSC_0021.JPG',3,'',5,1),(17,'2019-04-10 00:12:54.367450','9','User1: user_2/2019-04-10_024840.219469_name_DSC_0010.JPG',3,'',5,1),(18,'2019-04-10 00:12:54.371418','8','User1: user_2/2019-04-10_024303.130942_name_DSC_0003.JPG',3,'',5,1),(19,'2019-04-10 00:12:54.375386','7','User1: user_2/2019-04-10_024257.868391_name_DSC_0019.JPG',3,'',5,1),(20,'2019-04-10 00:12:54.378363','6','User1: user_2/2019-04-10_023827.835276_name_DSC_0060.JPG',3,'',5,1),(21,'2019-04-10 00:12:54.380347','5','User1: user_2/2019-04-10_023822.671924_name_DSC_0019.JPG',3,'',5,1),(22,'2019-04-10 00:12:54.386325','4','User1: user_2/2019-04-10_021414.475813_name_DSC_0013.JPG',3,'',5,1),(23,'2019-04-10 00:12:54.389274','3','User1: user_2/2019-04-10_021406.326546_name_DSC_0020.JPG',3,'',5,1),(24,'2019-04-10 00:12:54.393243','2','User1: user_2/2019-04-09_213621.922726_name_DSC_0008.JPG',3,'',5,1),(25,'2019-04-10 00:12:54.396218','1','User1: user_2/2019-04-09_213613.092926_name_DSC_0010.JPG',3,'',5,1),(26,'2019-04-10 00:13:03.383262','8','id: 8 username: User1',3,'',6,1),(27,'2019-04-10 00:13:03.385227','7','id: 7 username: User1',3,'',6,1),(28,'2019-04-10 00:13:03.388203','6','id: 6 username: User1',3,'',6,1),(29,'2019-04-10 00:13:03.392171','5','id: 5 username: User1',3,'',6,1),(30,'2019-04-10 00:13:03.395146','4','id: 4 username: User1',3,'',6,1),(31,'2019-04-10 00:13:03.397131','3','id: 3 username: User1',3,'',6,1),(32,'2019-04-10 00:13:03.400130','2','id: 2 username: User1',3,'',6,1),(33,'2019-04-10 00:13:03.403082','1','id: 1 username: User1',3,'',6,1),(34,'2019-04-10 18:36:45.862409','13','id: 13 username: User4',3,'',6,1),(35,'2019-04-10 18:36:45.866400','12','id: 12 username: User3',3,'',6,1),(36,'2019-04-10 18:36:45.869900','11','id: 11 username: User3',3,'',6,1),(37,'2019-04-10 18:36:45.872901','10','id: 10 username: User3',3,'',6,1),(38,'2019-04-10 18:36:45.880870','9','id: 9 username: User1',3,'',6,1),(39,'2019-04-10 18:37:03.699602','22','User4: user_4/2019-04-10_204130.548648_name_DSC_0039.JPG',3,'',5,1),(40,'2019-04-10 18:37:03.702620','21','User4: user_4/2019-04-10_204124.629844_name_DSC_0019.JPG',3,'',5,1),(41,'2019-04-10 18:37:03.705586','20','User3: user_3/2019-04-10_204046.127163_name_DSC_0002.JPG',3,'',5,1),(42,'2019-04-10 18:37:03.708578','19','User3: user_3/2019-04-10_203901.632043_name_DSC_0019.JPG',3,'',5,1),(43,'2019-04-10 18:37:03.711569','18','User3: user_3/2019-04-10_203834.394993_name_DSC_0001.JPG',3,'',5,1),(44,'2019-04-10 18:37:03.714589','16','User3: user_3/2019-04-10_202643.481334_name_DSC_0040.JPG',3,'',5,1),(45,'2019-04-10 18:37:03.716557','15','User3: user_3/2019-04-10_202637.697797_name_DSC_0062.JPG',3,'',5,1),(46,'2019-04-10 18:37:03.720545','14','User3: user_3/2019-04-10_202625.033659_name_DSC_0011.JPG',3,'',5,1),(47,'2019-04-10 18:37:03.722540','13','User1: user_2/2019-04-10_174206.470919_name_DSC_0022.JPG',3,'',5,1),(48,'2019-04-10 18:37:03.725532','12','User1: user_2/2019-04-10_174201.855260_name_DSC_0019.JPG',3,'',5,1),(49,'2019-04-10 18:37:49.879171','2','пр. Пушкина, дом. Колотушкина',3,'',4,1),(50,'2019-04-10 18:37:49.883160','1','пр. Победы, дом 26',3,'',4,1),(51,'2019-04-10 18:37:57.114490','2','Печать 4x3',3,'',2,1),(52,'2019-04-10 18:37:57.117481','1','Печать 10x15',3,'',2,1),(53,'2019-04-10 18:38:45.242370','3','Печать 30х45 Стандарт',1,'[{\"added\": {}}]',2,1),(54,'2019-04-10 18:39:04.022287','4','Печать 20х30 Стандарт',1,'[{\"added\": {}}]',2,1),(55,'2019-04-10 18:39:17.419647','5','Печать 15х21 Стандарт',1,'[{\"added\": {}}]',2,1),(56,'2019-04-10 18:39:34.615880','6','Печать 15х15 Стандарт',1,'[{\"added\": {}}]',2,1),(57,'2019-04-10 18:39:51.261801','7','Печать 13х18 Стандарт',1,'[{\"added\": {}}]',2,1),(58,'2019-04-10 18:40:15.707991','8','Печать 10х13,5 Стандарт',1,'[{\"added\": {}}]',2,1),(59,'2019-04-10 18:40:30.979193','9','Печать 10х15 Стандарт',1,'[{\"added\": {}}]',2,1),(60,'2019-04-10 18:42:19.978303','9','Печать 10х15 Стандарт',3,'',2,1),(61,'2019-04-10 18:42:19.981323','8','Печать 10х13,5 Стандарт',3,'',2,1),(62,'2019-04-10 18:42:19.983299','7','Печать 13х18 Стандарт',3,'',2,1),(63,'2019-04-10 18:42:19.987281','6','Печать 15х15 Стандарт',3,'',2,1),(64,'2019-04-10 18:42:19.990272','5','Печать 15х21 Стандарт',3,'',2,1),(65,'2019-04-10 18:42:19.993284','4','Печать 20х30 Стандарт',3,'',2,1),(66,'2019-04-10 18:42:19.997273','3','Печать 30х45 Стандарт',3,'',2,1),(67,'2019-04-10 18:42:27.521103','10','10х15 Шелк',1,'[{\"added\": {}}]',2,1),(68,'2019-04-10 18:42:38.791788','11','15х20 Шелк',1,'[{\"added\": {}}]',2,1),(69,'2019-04-10 18:42:48.920813','12','20х30 Шелк',1,'[{\"added\": {}}]',2,1),(70,'2019-04-10 18:42:58.431642','13','30х45 Шелк',1,'[{\"added\": {}}]',2,1),(71,'2019-04-10 18:44:41.393540','14','10х15 Стандарт',1,'[{\"added\": {}}]',2,1),(72,'2019-04-10 18:44:56.135988','15','30х45 Стандарт',1,'[{\"added\": {}}]',2,1),(73,'2019-04-10 18:45:10.787189','16','20х30 Стандарт',1,'[{\"added\": {}}]',2,1),(74,'2019-04-10 18:45:21.776403','17','13х18 Стандарт',1,'[{\"added\": {}}]',2,1),(75,'2019-04-10 18:47:12.945854','18','60x90 Пенокартон',1,'[{\"added\": {}}]',2,1),(76,'2019-04-10 18:47:45.970581','19','30x60 Пенокартон',1,'[{\"added\": {}}]',2,1),(77,'2019-04-10 18:48:33.705560','20','60x90 Накатка на пенокартон',1,'[{\"added\": {}}]',2,1),(78,'2019-04-10 18:49:32.879686','21','60x90 Накатка на пенокартон c золотым багетом',1,'[{\"added\": {}}]',2,1),(79,'2019-04-10 18:51:07.022728','3','Фотосалон YouPhoto, ул.Чванова дом.25',1,'[{\"added\": {}}]',4,1),(80,'2019-04-10 18:51:58.423041','4','Пункт выдачи YouPhoto, ул.Крылова дом.5',1,'[{\"added\": {}}]',4,1),(81,'2019-04-10 18:52:47.717468','5','Фотосалон YouPhoto, ул.Сергея Есенина дом.2',1,'[{\"added\": {}}]',4,1),(82,'2019-04-10 18:54:58.233173','6','Фотосалон YouPhoto, пр.Ильича дом.42',1,'[{\"added\": {}}]',4,1),(83,'2019-04-10 19:01:40.698471','6','Manager2',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',9,1),(84,'2019-04-10 19:01:57.070513','5','Manager1',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',9,1),(85,'2019-04-10 19:54:10.123766','1','Manager',1,'[{\"added\": {}}]',8,1),(86,'2019-04-10 19:54:21.633990','5','Manager1',2,'[{\"changed\": {\"fields\": [\"groups\"]}}]',9,1),(87,'2019-04-10 19:54:28.192454','6','Manager2',2,'[{\"changed\": {\"fields\": [\"groups\"]}}]',9,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (11,'admin','logentry'),(8,'auth','group'),(7,'auth','permission'),(9,'auth','user'),(10,'contenttypes','contenttype'),(12,'sessions','session'),(1,'website','delivery'),(6,'website','order'),(5,'website','picture'),(4,'website','place'),(2,'website','service'),(3,'website','status');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-04-09 17:52:45.963262'),(2,'auth','0001_initial','2019-04-09 17:52:46.185478'),(3,'website','0001_initial','2019-04-09 17:52:47.105052'),(4,'admin','0001_initial','2019-04-09 17:52:51.597944'),(5,'admin','0002_logentry_remove_auto_add','2019-04-09 17:52:51.901522'),(6,'admin','0003_logentry_add_action_flag_choices','2019-04-09 17:52:51.919351'),(7,'contenttypes','0002_remove_content_type_name','2019-04-09 17:52:52.416343'),(8,'auth','0002_alter_permission_name_max_length','2019-04-09 17:52:52.581041'),(9,'auth','0003_alter_user_email_max_length','2019-04-09 17:52:52.619702'),(10,'auth','0004_alter_user_username_opts','2019-04-09 17:52:52.635581'),(11,'auth','0005_alter_user_last_login_null','2019-04-09 17:52:52.775446'),(12,'auth','0006_require_contenttypes_0002','2019-04-09 17:52:52.783382'),(13,'auth','0007_alter_validators_add_error_messages','2019-04-09 17:52:52.800246'),(14,'auth','0008_alter_user_username_max_length','2019-04-09 17:52:52.974837'),(15,'auth','0009_alter_user_last_name_max_length','2019-04-09 17:52:53.139510'),(16,'auth','0010_alter_group_name_max_length','2019-04-09 17:52:53.168277'),(17,'auth','0011_update_proxy_permissions','2019-04-09 17:52:53.185171'),(18,'sessions','0001_initial','2019-04-09 17:52:53.259541'),(19,'website','0002_order_price','2019-04-09 21:10:16.426996'),(20,'website','0003_auto_20190410_0200','2019-04-09 23:00:38.296243'),(21,'website','0004_auto_20190410_0202','2019-04-09 23:02:29.622094'),(22,'website','0005_auto_20190410_0242','2019-04-09 23:42:26.975694'),(23,'website','0006_auto_20190410_0244','2019-04-09 23:44:53.291221'),(24,'website','0007_auto_20190410_2206','2019-04-10 19:07:03.849565');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('d12jkljtty35x0ayqgaaykbfg7quvcis','ZmMyMjhlZjUwODcyZWI5NmNiZTI4NmIyNzc0ZDY5ZmFmZGNmZTZkMzp7Il9hdXRoX3VzZXJfaWQiOiI1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlOTM3M2QzMDk0ZjVmMDI5YWRmODg5MmJiYzZiZTczYzBmMzBjNTYyIn0=','2019-04-24 20:56:52.336643');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_delivery`
--

DROP TABLE IF EXISTS `website_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_delivery`
--

LOCK TABLES `website_delivery` WRITE;
/*!40000 ALTER TABLE `website_delivery` DISABLE KEYS */;
INSERT INTO `website_delivery` VALUES (1,'Почтой',200.00),(2,'Само-вывоз',0.00),(3,'Курьером',350.00);
/*!40000 ALTER TABLE `website_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_order`
--

DROP TABLE IF EXISTS `website_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentary` longtext NOT NULL,
  `delivery_place` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `subpoint_delivery_id` int(11) DEFAULT NULL,
  `price` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `website_order_delivery_id_72f7d08f_fk_website_delivery_id` (`delivery_id`),
  KEY `website_order_owner_id_ff9e468e_fk_auth_user_id` (`owner_id`),
  KEY `website_order_status_id_dfd3c6dc_fk_website_status_id` (`status_id`),
  KEY `website_order_subpoint_delivery_id_1bfc6418_fk_website_place_id` (`subpoint_delivery_id`),
  CONSTRAINT `website_order_delivery_id_72f7d08f_fk_website_delivery_id` FOREIGN KEY (`delivery_id`) REFERENCES `website_delivery` (`id`),
  CONSTRAINT `website_order_owner_id_ff9e468e_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `website_order_status_id_dfd3c6dc_fk_website_status_id` FOREIGN KEY (`status_id`) REFERENCES `website_status` (`id`),
  CONSTRAINT `website_order_subpoint_delivery_id_1bfc6418_fk_website_place_id` FOREIGN KEY (`subpoint_delivery_id`) REFERENCES `website_place` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_order`
--

LOCK TABLES `website_order` WRITE;
/*!40000 ALTER TABLE `website_order` DISABLE KEYS */;
INSERT INTO `website_order` VALUES (14,'Комментарий к заказу','Адрес проживания','+7(930)708-96-50','zona131313@gmail.com','Александр',3,2,1,NULL,57150.00),(15,'','123456','+7(930)708-96-50','zona131313@gmail.com','Андрей',1,2,3,4,890.00),(16,'','Пункт выдачи YouPhoto, ул.Крылова дом.5','+7(930)708-96-50','zona131313@gmail.com','Алексей',2,7,2,NULL,275000.00),(17,'','Фотосалон YouPhoto, пр.Ильича дом.42','+7(930)708-96-50','zona131313@gmail.com','Владимир',2,7,1,NULL,13660.00),(18,'','654321','+7(930)708-96-50','zona131313@gmail.com','Роман',1,7,1,NULL,229.00),(19,'','Адрес','+7(930)708-96-50','zona131313@gmail.com','Алексей',3,3,1,NULL,1550.00),(20,'','Пункт выдачи YouPhoto, ул.Крылова дом.5','+7(930)708-96-50','zona131313@gmail.com','уфцыа',2,3,1,NULL,29.00),(21,'','Фотосалон YouPhoto, ул.Сергея Есенина дом.2','+7(930)708-96-50','zona131313@gmail.com','пргол',2,3,1,NULL,26000.00),(22,'','Фотосалон YouPhoto, ул.Чванова дом.25','+7(930)708-96-50','zona131313@gmail.com','Андрей',2,4,1,NULL,152.00),(23,'','Улица Пушкина','+7(930)708-96-50','zona131313@gmail.com','Михаил',3,4,1,NULL,379.00),(24,'','469785','+7(930)708-96-50','zona131313@gmail.com','Михаил',1,4,1,NULL,229.00),(25,'','984521','+7(930)708-96-50','zona131313@gmail.com','Андрей',1,4,1,NULL,258.00),(26,'','Фотосалон YouPhoto, ул.Сергея Есенина дом.2','+7(930)708-96-50','zona131313@gmail.com','Aleksandr',2,2,1,NULL,29.00);
/*!40000 ALTER TABLE `website_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_order_pic`
--

DROP TABLE IF EXISTS `website_order_pic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_order_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `picture_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `website_order_pic_order_id_picture_id_d170e69d_uniq` (`order_id`,`picture_id`),
  KEY `website_order_pic_picture_id_3fe91818_fk_website_picture_id` (`picture_id`),
  CONSTRAINT `website_order_pic_order_id_17b2f48e_fk_website_order_id` FOREIGN KEY (`order_id`) REFERENCES `website_order` (`id`),
  CONSTRAINT `website_order_pic_picture_id_3fe91818_fk_website_picture_id` FOREIGN KEY (`picture_id`) REFERENCES `website_picture` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_order_pic`
--

LOCK TABLES `website_order_pic` WRITE;
/*!40000 ALTER TABLE `website_order_pic` DISABLE KEYS */;
INSERT INTO `website_order_pic` VALUES (22,14,23),(23,14,24),(24,15,25),(25,15,26),(26,16,27),(27,17,28),(28,17,29),(29,18,30),(30,19,31),(31,20,32),(32,21,33),(33,22,34),(34,22,35),(35,22,36),(36,22,37),(37,22,38),(38,23,39),(39,24,40),(40,25,41),(41,25,42),(42,26,43);
/*!40000 ALTER TABLE `website_order_pic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_picture`
--

DROP TABLE IF EXISTS `website_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic` varchar(100) NOT NULL,
  `is_ordered` tinyint(1) NOT NULL,
  `count` int(11) NOT NULL,
  `commentary` longtext NOT NULL,
  `job_place_id` int(11) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `website_picture_job_place_id_05797a91_fk_website_place_id` (`job_place_id`),
  KEY `website_picture_owner_id_59997160_fk_auth_user_id` (`owner_id`),
  KEY `website_picture_service_id_bac68e16_fk_website_service_id` (`service_id`),
  CONSTRAINT `website_picture_job_place_id_05797a91_fk_website_place_id` FOREIGN KEY (`job_place_id`) REFERENCES `website_place` (`id`),
  CONSTRAINT `website_picture_owner_id_59997160_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `website_picture_service_id_bac68e16_fk_website_service_id` FOREIGN KEY (`service_id`) REFERENCES `website_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_picture`
--

LOCK TABLES `website_picture` WRITE;
/*!40000 ALTER TABLE `website_picture` DISABLE KEYS */;
INSERT INTO `website_picture` VALUES (23,'user_2/2019-04-10_220331.938941_name_0001.JPG',1,10,'Комментарий к первому фото',NULL,2,21),(24,'user_2/2019-04-10_220337.030899_name_0002.JPG',1,15,'Комментарий ко второму фото',NULL,2,15),(25,'user_2/2019-04-10_221356.128969_name_0007.JPG',1,5,'',3,2,15),(26,'user_2/2019-04-10_221401.220055_name_0010.JPG',1,5,'',6,2,17),(27,'user_7/2019-04-10_221511.104403_name_0002.JPG',1,50,'',3,7,21),(28,'user_7/2019-04-10_221550.823502_name_0006.JPG',1,20,'',NULL,7,14),(29,'user_7/2019-04-10_221554.744780_name_0008.JPG',1,5,'',NULL,7,18),(30,'user_7/2019-04-10_221648.284962_name_0002.JPG',1,1,'',NULL,7,10),(31,'user_3/2019-04-10_221730.936557_name_0002.JPG',1,10,'',NULL,3,15),(32,'user_3/2019-04-10_221823.287426_name_0001.JPG',1,1,'',NULL,3,10),(33,'user_3/2019-04-10_221839.993600_name_0008.JPG',1,20,'',NULL,3,19),(34,'user_4/2019-04-10_221908.735317_name_0010.JPG',1,1,'',NULL,4,14),(35,'user_4/2019-04-10_221912.567794_name_0007.JPG',1,1,'',NULL,4,14),(36,'user_4/2019-04-10_221917.475498_name_0008.JPG',1,1,'',NULL,4,14),(37,'user_4/2019-04-10_221924.362435_name_0009.JPG',1,1,'',NULL,4,14),(38,'user_4/2019-04-10_221930.836453_name_0002.JPG',1,1,'',NULL,4,15),(39,'user_4/2019-04-10_222024.368750_name_0003.JPG',1,1,'',NULL,4,10),(40,'user_4/2019-04-10_222057.176831_name_0002.JPG',1,1,'',NULL,4,10),(41,'user_4/2019-04-10_222227.215901_name_0006.JPG',1,1,'',NULL,4,10),(42,'user_4/2019-04-10_222231.573998_name_0007.JPG',1,1,'',NULL,4,10),(43,'user_2/2019-04-10_235309.391673_name_0010.JPG',1,1,'',NULL,2,10);
/*!40000 ALTER TABLE `website_picture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_place`
--

DROP TABLE IF EXISTS `website_place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adress` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_place`
--

LOCK TABLES `website_place` WRITE;
/*!40000 ALTER TABLE `website_place` DISABLE KEYS */;
INSERT INTO `website_place` VALUES (3,'Фотосалон YouPhoto, ул.Чванова дом.25'),(4,'Пункт выдачи YouPhoto, ул.Крылова дом.5'),(5,'Фотосалон YouPhoto, ул.Сергея Есенина дом.2'),(6,'Фотосалон YouPhoto, пр.Ильича дом.42');
/*!40000 ALTER TABLE `website_place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_place_services`
--

DROP TABLE IF EXISTS `website_place_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_place_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `website_place_services_place_id_service_id_56d51192_uniq` (`place_id`,`service_id`),
  KEY `website_place_services_service_id_ebabb015_fk_website_service_id` (`service_id`),
  CONSTRAINT `website_place_services_place_id_dd3d48fa_fk_website_place_id` FOREIGN KEY (`place_id`) REFERENCES `website_place` (`id`),
  CONSTRAINT `website_place_services_service_id_ebabb015_fk_website_service_id` FOREIGN KEY (`service_id`) REFERENCES `website_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_place_services`
--

LOCK TABLES `website_place_services` WRITE;
/*!40000 ALTER TABLE `website_place_services` DISABLE KEYS */;
INSERT INTO `website_place_services` VALUES (4,3,10),(5,3,11),(6,3,12),(7,3,13),(8,3,14),(9,3,15),(10,3,16),(11,3,17),(12,3,18),(13,3,19),(14,3,20),(15,3,21),(16,5,10),(17,5,11),(18,5,12),(19,5,13),(20,5,14),(21,5,15),(22,5,16),(23,5,17),(26,6,14),(27,6,15),(24,6,16),(25,6,17);
/*!40000 ALTER TABLE `website_place_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_service`
--

DROP TABLE IF EXISTS `website_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_service`
--

LOCK TABLES `website_service` WRITE;
/*!40000 ALTER TABLE `website_service` DISABLE KEYS */;
INSERT INTO `website_service` VALUES (10,'10х15 Шелк',29.00),(11,'15х20 Шелк',69.00),(12,'20х30 Шелк',139.00),(13,'30х45 Шелк',279.00),(14,'10х15 Стандарт',8.00),(15,'30х45 Стандарт',120.00),(16,'20х30 Стандарт',60.00),(17,'13х18 Стандарт',18.00),(18,'60x90 Пенокартон',2700.00),(19,'30x60 Пенокартон',1300.00),(20,'60x90 Накатка на пенокартон',4700.00),(21,'60x90 Накатка на пенокартон c золотым багетом',5500.00);
/*!40000 ALTER TABLE `website_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_status`
--

DROP TABLE IF EXISTS `website_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `website_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_status`
--

LOCK TABLES `website_status` WRITE;
/*!40000 ALTER TABLE `website_status` DISABLE KEYS */;
INSERT INTO `website_status` VALUES (1,'Новый'),(2,'В работе'),(3,'В пути'),(4,'Ждет оплаты'),(5,'Готов к выдаче'),(6,'Завершен'),(7,'Отменен');
/*!40000 ALTER TABLE `website_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-11  0:02:58
