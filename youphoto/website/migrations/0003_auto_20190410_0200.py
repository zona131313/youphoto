# Generated by Django 2.2 on 2019-04-09 23:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_order_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='subpoint_delivery',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='website.Place'),
        ),
    ]
