from django.test import TestCase
from django.contrib.auth.models import Group
from .views import *


class ViewTests(TestCase):
    def setUp(self):
        manager = User()
        client = User()
        manager.username = "Manager"
        client.username = "Client"
        manager.password = "testpas"
        client.password = "testpas"
        Group.objects.create(name="Manager").save()
        manager.save()
        manager.groups.add(Group.objects.get(name="Manager"))
        manager.save()
        client.save()

    def test_unitTestIndexView(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code == 200)
        pass

    def test_unitTestNewUserView(self):
        username = "User1"
        password = "qwerty12345"
        response = self.client.post('/signup/', {'username': username, 'password1': password, 'password2': password})
        self.assertTrue(response.status_code == 200)
        pass

    def test_unitTestLoginView(self):
        response = self.client.post('/login/', {'username': 'User1', 'password': 'qwerty12345'})
        print(response.status_code)
        # self.assertRedirects(response, '/user/')
        self.assertTrue(response.status_code == 302)
    #
    # def test_unitTestUserView(self):
    #     self.client.post('/login/', {'username': 'Client', 'password': 'testpas'})
    #     response = self.client.get('/user/')
    #     self.assertTrue(response.status_code == 200)

    def test_unitTestNegativeUserView(self):
        response = self.client.get('/user/')
        self.assertTrue(response.status_code == 302)
        pass

    # def test_unitTestManagerLoginView(self):
    #      return False

    def test_unitTestNegativeManagerLoginView(self):
        response = self.client.get('/manager/')
        self.assertTrue(response.status_code == 403)
        pass
