from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, date, time


def user_directory_path(instance, filename):
    return 'user_{0}/{1}_name_{2}'.format(instance.owner.id, datetime.now(), filename)

def status_default():
    if not Status.objects.all().exists():
        Status.objects.create(name="none")
    return Status.objects.first()


class Service(models.Model):
    name = models.CharField(max_length=255, null=False)
    price = models.DecimalField(decimal_places=2, max_digits=12, default=0.00)

    def __str__(self):
        return self.name


class Place(models.Model):
    adress = models.CharField(max_length=255, null=False)
    services = models.ManyToManyField(Service, blank=True)

    def __str__(self):
        return self.adress


class Delivery(models.Model):
    name = models.CharField(max_length=255, null=False)
    price = models.DecimalField(decimal_places=2, max_digits=12, default=0.00)

    def __str__(self):
        return self.name

class Picture(models.Model):
     owner = models.ForeignKey(User, on_delete=models.CASCADE)
     pic = models.FileField(upload_to=user_directory_path)
     is_ordered = models.BooleanField(default=False)
     service = models.ForeignKey(Service, on_delete=models.CASCADE, blank=True, null=True)
     job_place = models.ForeignKey(Place, on_delete=models.CASCADE, blank=True, null=True)
     count = models.IntegerField(default=1)
     commentary = models.TextField(blank=True, default="")

     def __str__(self):
         return self.owner.username + ": " + self.pic.name

class Status(models.Model):
    name = models.CharField(max_length=255, null=False)

    def __str__(self):
        return "id: {0} name: {1}".format(self.id, self.name)


class Order(models.Model):
    pic = models.ManyToManyField(Picture, blank=True)
    commentary = models.TextField()
    status = models.ForeignKey(Status, on_delete=models.CASCADE, default=status_default)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    delivery = models.ForeignKey(Delivery, on_delete=models.CASCADE)
    subpoint_delivery = models.ForeignKey(Place, on_delete=models.CASCADE, blank=True, null=True)
    delivery_place = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=255, blank=True, null=True, default="")
    name = models.CharField(max_length=255)
    price = models.DecimalField(decimal_places=2, max_digits=12, default=0.00)

    def __str__(self):
        return "id: {0} username: {1}".format(self.id, self.owner.username)
