from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

admin.site.index_template = 'admin/website/index.html'
admin.autodiscover()

urlpatterns = [
    path('', views.index, name='index'),
    path('addorder/', views.addorder, name='addorder'),
    path('login/', views.my_login, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='website/index.html'), name='logout'),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('user_order_status/<int:id>', views.user_order_status, name='user_order_status'),
    path('change_status/<int:order_id>/<int:status_id>', views.change_status, name='change_status'),
    path('manager/', views.manager, name='manager'),
    path('signup_success/', views.signup_success, name='signup_success'),
    path('login_failed/', views.login_failed, name='login_failed'),
    path('check_username/<str:username_check>', views.check_username, name='check_username'),
    path('manager/', views.manager, name='manager'),
    path('user/', views.user, name='user'),
    path('delete_image/<int:image_id>', views.delete_image, name='delete_image'),
    path('orderstatus/', views.orderstatus, name='orderstatus'),
    path('statistic/', views.statistic, name='staitstic'),
    path('checkorder/<int:order_id>', views.checkorder, name='checkorder'),
    path('neworder/', views.neworder, name='neworder'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
