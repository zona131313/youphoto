from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.template import loader
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from .models import *
from django.core.paginator import Paginator
from functools import wraps
from django.contrib.auth.decorators import login_required



def manager_only(function):
  @wraps(function)
  def wrap(request, *args, **kwargs):
    if request.user.is_authenticated:
        if request.user.groups.filter(name='Manager').exists() or request.user.is_superuser:
             return function(request, *args, **kwargs)
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseForbidden()

  return wrap


def index(request):
    if request.user.is_authenticated:
        if request.user.groups.filter(name='Manager').exists():
            return HttpResponseRedirect("/manager")
        else:
            return HttpResponseRedirect("/user")
    else:
        template = loader.get_template('website/index.html')
        return HttpResponse(template.render({}, request))


@manager_only
def manager(request):
    if request.method == 'GET' and request.user.is_authenticated:
        template = loader.get_template('website/manager.html')
        orders = Order.objects.all().order_by('status')
        paginator = Paginator(orders, 8)

        page = request.GET.get('page')

        orders = paginator.get_page(page)
        status = Status.objects.all()
        context = {
            'orders': orders,
            'status': status,
        }

        return HttpResponse(template.render(context, request))
    if request.method == 'POST' and request.user.is_authenticated:
        order = Order.objects.get(id=request.POST.get('order-id'))
        id_subpoint = request.POST.get('subpoint')
        if id_subpoint:
            order.subpoint_delivery = Place.objects.get(id=id_subpoint)
        order.status = Status.objects.get(id=request.POST.get('status'))
        pictures = order.pic.all()
        for picture in pictures:
            picture.job_place = Place.objects.get(id=request.POST.get('job-place-{0}'.format(picture.id)))
            picture.save()
        order.save()
        return HttpResponseRedirect('/manager')


@manager_only
def change_status(request, order_id, status_id):
    order = Order.objects.get(id=order_id)
    order.status = Status.objects.get(id=status_id)
    order.save()
    return HttpResponse(order.status.name)


@login_required(login_url="/")
def addorder(requset):
    if requset.method == 'POST' and requset.user.is_authenticated:
            neworder = Order()
            neworder.owner = requset.user
            neworder.commentary = requset.POST.get('comment')
            neworder.name = requset.POST.get('username')
            neworder.email = requset.POST.get('email')
            neworder.phone = requset.POST.get('phone')
            neworder.delivery = Delivery.objects.get(id=requset.POST.get('delivery'))
            if neworder.delivery.name == 'Само-вывоз':
                neworder.delivery_place = Place.objects.get(id=requset.POST.get('place')).adress
            else:
                if neworder.delivery.name == "Почтой":
                    neworder.delivery_place = requset.POST.get('mail')
                else:
                    neworder.delivery_place = requset.POST.get('adress')
            price = neworder.delivery.price
            pics = Picture.objects.filter(owner=requset.user, is_ordered=False)
            for pic in pics:
                price += pic.service.price * pic.count
            neworder.price = price
            neworder.save()
            try:
                for pic in pics:
                    neworder.pic.add(pic)
                    pic.is_ordered = True
                for pic in pics:
                    pic.save()
                neworder.save()
            except AssertionError:
                neworder.delete()
            return HttpResponseRedirect('/orderstatus')
    else:
        return HttpResponseRedirect('/user')


@login_required(login_url="/")
def neworder(request):
    if request.method == 'POST' and request.user.is_authenticated:
        pictures = Picture.objects.filter(owner=request.user, is_ordered=False)
        for picture in pictures:
            count = request.POST.get('count_{0}'.format(picture.id))
            if int(count) < 1:
                return HttpResponseRedirect('/user')
            picture.count = int(count)
            picture.commentary = request.POST.get('comment_{0}'.format(picture.id))
            service = Service.objects.get(id=request.POST.get('select_{0}'.format(picture.id)))
            if service:
                picture.service = service
            else:
                HttpResponseRedirect('/user')

        places = Place.objects.all()
        deliverys = Delivery.objects.all()

        if places and deliverys:
            price = 0
            for picture in pictures:
                price += picture.count * picture.service.price
                picture.save()
            context = {
                'pictures': pictures,
                'places': places,
                'deliverys': deliverys,
                'price': price,
            }

            template = loader.get_template('website/neworder.html')
            return HttpResponse(template.render(context, request))
        else:
            return HttpResponseRedirect('user')
    else:
        return HttpResponseRedirect('/')


@manager_only
def checkorder(request, order_id):
    template = loader.get_template('website/checkorder.html')
    order = Order.objects.get(id=order_id)
    places = Place.objects.all()
    status = Status.objects.all()
    context = {
        'order': order,
        'places': places,
        'status': status,
    }

    return HttpResponse(template.render(context, request))


@manager_only
def statistic(request):
    template = loader.get_template('website/statistic.html')
    orders = Order.objects.all()

    orders_count = orders.count()

    orders_sum_cash = 0
    orders_fail_cash = 0
    orders_future_cash = 0
    order_success = 0
    order_fails = 0
    order_starts = 0
    other_orders = 0

    for order in orders:
        if order.status.name == "Завершен":
            order_success += 1
            orders_sum_cash += order.price
        else:
            if order.status.name == "Отменен":
                order_fails += 1
                orders_fail_cash += order.price
            else:
                if order.status.name == "Новый":
                    order_starts += 1
                    orders_future_cash += order.price
                else:
                    other_orders += 1
                    orders_future_cash += order.price


    context = {
        'orders_count': orders_count,
        'orders_sum_cash': orders_sum_cash,
        'orders_fail_cash': orders_fail_cash,
        'orders_future_cash': orders_future_cash,
        'order_success': order_success,
        'order_fails': order_fails,
        'order_starts': order_starts,
        'other_orders': other_orders,
    }
    return HttpResponse(template.render(context, request))


@login_required(login_url="/")
def orderstatus(request):
    template = loader.get_template('website/orderstatus.html')
    orders = Order.objects.filter(owner=request.user).order_by('-id')
    paginator = Paginator(orders, 8)

    page = request.GET.get('page')

    orders = paginator.get_page(page)

    context = {
        'orders': orders,
    }
    return HttpResponse(template.render(context, request))


def user_order_status(request, id):
    order = Order.objects.get(id=id)
    if order.owner == request.user:
        template = loader.get_template("website/user_order_status_template.html")
        context = {
            'order': order
        }
        return HttpResponse(template.render(context, request))
    else:
        return HttpResponse("404")


@login_required(login_url="/")
def user(request):
    if(request.method == 'GET'):
        template = loader.get_template('website/user.html')
        pictures = Picture.objects.filter(owner=request.user, is_ordered=False)
        services = Service.objects.all()
        context = {
               'pictures': pictures,
               'services': services,
        }
        return HttpResponse(template.render(context, request))
    if(request.method == 'POST'):
        new_picture = Picture()
        new_picture.owner = request.user
        new_picture.pic = request.FILES['image']
        new_picture.save()
        return HttpResponseRedirect('/user')


def delete_image(request, image_id):
    pic = Picture.objects.get(id=image_id)
    if(request.user.is_authenticated and pic.owner == request.user):
        pic.delete()
    return HttpResponseRedirect('/user')


def check_username(request, username_check):
    try:
        user = User.objects.get(username=username_check)
        return HttpResponse("Имя пользователя занято, выберите другое")
    except User.DoesNotExist:
        return HttpResponse("Имя пользователя свободно")


def my_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)

    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        return redirect('/login_failed/')


def login_failed(request):
    template = loader.get_template('website/login_failed.html')
    return HttpResponse(template.render({}, request))


def signup_success(request):
    template = loader.get_template('website/signup_success.html')
    return HttpResponse(template.render({}, request))


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('signup_success')
    template_name = 'website/index.html'
