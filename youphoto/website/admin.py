from django.contrib import admin
from .models import Service
from .models import Place
from .models import Order
from .models import Delivery
from .models import Status
from .models import Picture
# Register your models here.

admin.site.register(Service)
admin.site.register(Place)
admin.site.register(Delivery)
admin.site.register(Status)
admin.site.register(Order)
admin.site.register(Picture)