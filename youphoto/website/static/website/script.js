function check_submit(username, pass, confirm) {
    if (username && pass && confirm){
        $("#register_submit").removeAttr("disabled");
    } else {
        $("#register_submit").prop("disabled", true);
    }
}

function check_access(is_user_exist) {
    if (is_user_exist) {
        $("#give_access").removeClass("disabled");
    } else {
        $("#give_access").addClass("disabled");
    }
}

function update_image(input){
    if(input.files && input.files[0]){
        if(input.files[0].type == "image/jpeg" || input.files[0].type == "image/png") {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#downloaded_image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
            $('#downloaded_image').removeAttr('hidden');
            $('#download_image').removeAttr('disabled');
        } else{
            $('#downloaded_image').prop('hidden', true);
            $('#download_image').prop('disabled', true);
            $("#id_image").val("")
        }
    }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function change_status(order_id) {
    var status = $("#select_".concat(order_id));
    $.get("/change_status/".concat(order_id + "/" + status.val()), function (data, status) {
        $("#status-".concat(order_id)).html(data)
    });
}

function delivery_change(price) {
    var id = $("#iddelivery").val();
    var option = $("#delivery_".concat(id));
    var name = option.attr('data-name');
    var delivery_price = option.attr('data-price');
    var adress = $("#adress");
    var place = $("#idplace");
    var mail = $("#mail");
    adress.val("");
    mail.val("");
        if(name == "Почтой"){
            mail.show();
            mail.prop('required', true);
            adress.hide();
            adress.removeAttr('required');
            place.removeClass("custom-select d-block w-100");
            place.hide();
        }
        else if(name == "Само-вывоз"){
            mail.hide();
            mail.removeAttr('required');
            adress.hide();
            adress.removeAttr('required');
            place.addClass("custom-select d-block w-100");
            place.show();
        }
        else{
            adress.show();
            adress.prop('required', true);
            mail.hide();
            mail.removeAttr('required');
            place.removeClass("custom-select d-block w-100");
            place.hide();
        }
        var allprice = parseFloat(delivery_price) + parseFloat(price);
        $("#price-text").html("Стоймость заказа: ".concat(allprice + "₽"));
}

function more(id) {
   $.get("/user_order_status/" + id, function (data, status) {
       $("#more").html(data);
   })
}

// For Register
var csrftoken = getCookie('csrftoken');
var username = false;
var pass = false;
var confirm = false;

var is_user_exist=false;

$(document).ready(function(){
    if ($("#id_is_free").prop('checked') == false) {
        $("#access").removeAttr("hidden");
    }

    $("#id_username1").blur(function() {
        $.get("/check_username/" + $("#id_username1").val(), function(data, status){
            if (data.match("свободно")) {
                $("#username_success").html(data);
                $("#username_error").html("");
                username = true;
                check_submit(username, pass, confirm);
            } else {
                $("#username_success").html("");
                $("#username_error").html(data);
                username = false;
                check_submit(username, pass, confirm);
            }
        });
    });

    $("#id_password1").blur(function(){
        if ($("#id_password1").val().length >= 8) {
            $("#password_success").html("Пароль удовлетворяет требованиям");
            $("#password_error").html("");
            pass = true;
            check_submit(username, pass, confirm);
        } else {
            $("#password_success").html("");
            $("#password_error").html("Длина пароля должна быть не менее 8 символов!");
            pass = false;
            check_submit(username, pass, confirm);
        }
    });

    $("#id_password2").blur(function(){
        if ($("#id_password1").val() == $("#id_password2").val()) {
            $("#confirm_success").html("Пароли совпадают");
            $("#confirm_error").html("");
            confirm = true;
            check_submit(username, pass, confirm);
        } else {
            $("#confirm_success").html("");
            $("#confirm_error").html("Пароли не совпадают");
            confirm = false;
            check_submit(username, pass, confirm);
        }
    });

    $("#access_user").blur(function() {
        $("#access_waiting").html("Ищем пользователя...");
        $("#access_success").html("");
        $("#access_error").html("");
        $.get("/check_username/" + $("#access_user").val(), function(data, status){
            if (data.match("свободно")) {
                $("#access_success").html("");
                $("#access_error").html("Пользователя с таким именем не существует, попробуйте еще раз...");
                $("#access_waiting").html("");
                is_user_exist = false;
                check_access(is_user_exist)
            } else {
                $("#access_success").html("Пользователь с таким именем существует.");
                $("#access_error").html("");
                $("#access_waiting").html("");
                is_user_exist = true;
                check_access(is_user_exist);
            }
        });
    });

    $("#idphone").mask("+7(999)999-99-99");

    $("#id_image").change(function () {
        update_image(this);
    })
});
